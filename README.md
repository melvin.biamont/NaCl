# ACL management

Why this logo ?

Na+ Cl- is the nomenclature of the salt.

## Import

```bash
go get gitlab.com/melvin.biamont/NaCl
```

## Setup

### Describe your ACL configuration

```json
[
  {
    "role": "YOUR_ROLE_NAME",
    "permissions": [
      {
        "resource": "*",
        "method": "PATCH", 
        "action": "deny"
      },
      {
        "resource": "*",
        "method": "*",
        "action": "allow"
      }
    ]
  }
]
```

### Add a middleware

```go
func main() {

    (...) // Setup your http handler here

    acl, err := nacl.New("CONFIG_PATH")

	if err != nil {
		panic(err)
	}
	
	handler = acl.Authenticate(Authenticator).
	    OnDenied(OnDenied).
	    Handler(handler) //Put your original http handler here
	    
	http.ListenAndServe(":8080", handler)
}

func Authenticator(nacl *nacl.Acl, w http.ResponseWriter, r *http.Request) *nacl.Role {
    //Do your logic here to know which role is the user (using HTTP headers, userID, etc...)
}

func OnDenied(w http.ResponseWriter, r *http.Request, role *nacl.Role) {
	http.Error(w, "You're not authorized to access to this page.", http.StatusUnauthorized)
}

```