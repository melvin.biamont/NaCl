package nacl

import (
	"net/http"
	"io/ioutil"
	"encoding/json"
	"fmt"
)

type authenticate func(nacl *Acl, w http.ResponseWriter, r *http.Request) *Role
type handle func(w http.ResponseWriter, r *http.Request) *Role
type Deny func(w http.ResponseWriter, r *http.Request, role *Role)

type Acl struct {
	roles map[string]Role

	auth   authenticate
	denier *Deny
}

func New(configPath string) (*Acl, error) {
	file, err := ioutil.ReadFile(configPath)

	if err != nil {
		return nil, err
	}

	a := Acl{}

	var roles []Role

	if err := json.Unmarshal(file, &roles); err != nil {
		return nil, err
	}

	a.roles = make(map[string]Role)
	for _, r := range roles {
		a.roles[r.Name] = r
	}

	err = a.compileMatchers()

	if err != nil {
		return nil, err
	}

	return &a, a.validateConfig()
}

func (a *Acl) compileMatchers() error {
	for _, role := range a.roles {
		for _, perm := range role.Permissions {
			err := perm.compileMatcher()
			if err != nil {
				return err
			}
		}
	}
	return nil
}

func (a Acl) validateConfig() error {
	//TODO VERIFY NO CONFLICT IN CONFIG
	return nil
}

func (a *Acl) GetRole(name string) *Role {
	r, ok := a.roles[name]

	if !ok {
		return nil
	}
	return &r
}

func (a *Acl) accept(w http.ResponseWriter, r *http.Request) bool {
	role := a.auth(a, w, r)
	fmt.Printf("ACL: Role %s\n", role.Name)

	if role == nil {
		return false
	}

	for _, perm := range role.Permissions {
		if perm.matchResource(r) && perm.matchMethod(r) {
			if perm.isAllowed(r) {
				fmt.Printf("ACL: Route matching. Allowed.\n")
				return true
			} else {
				if a.denier != nil {
					fmt.Printf("ACL: Route matching. Denied.\n")
					d := *(a.denier)
					d(w, r, role)
				} else {
					fmt.Printf("ACL: Route matching. Denied.\n")
				}
				return false
			}
		}
	}
	fmt.Printf("ACL: No Route %s matching\n", r.URL.String())
	return false
}

func (a *Acl) Handler(h http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		if a.accept(w, r) {
			h.ServeHTTP(w, r)
		}
	})
}

func (a *Acl) OnDenied(deny Deny) *Acl {
	a.denier = &deny
	return a
}

func (a *Acl) Authenticate(auth authenticate) *Acl {
	a.auth = auth
	return a
}
