package nacl

import (
	"testing"
	"fmt"
)

func TestAuthenticateNoExists(t *testing.T){
	a, err := New("sample/acl.json")

	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	role := a.GetRole("ROLE_NOT_EXISTING")

	if role != nil {
		fmt.Printf("Who is %s ???\n", role.Name)
		t.FailNow()
	}
}

func TestAuthenticateExists(t *testing.T){
	a, err := New("sample/acl.json")

	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	role := a.GetRole("SUPERADMIN")

	if role == nil {
		t.FailNow()
	}
}

func TestConfigOk(t *testing.T) {
	a, err := New("sample/acl.json")

	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	if a == nil {
		t.FailNow()
	}

	for _, role := range a.roles {
		fmt.Printf("Role : %s\n", role.Name)

		for _, permission := range role.Permissions {
			fmt.Printf("Action : %s\n", permission.Action)
			fmt.Printf("Resource : %s\n", permission.Resource)

			if permission.Method != "" {
				fmt.Printf("Method : %s\n", permission.Method)
			} else {
				for _, method := range permission.Methods {
					fmt.Printf("Method : %s\n", method)
				}
			}
		}
		fmt.Printf("---\n")
	}

}
