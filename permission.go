package nacl

import (
	"net/http"
	"strings"
	"regexp"
)

const ACTION_ALLOW = "allow"

type Permission struct {
	Resource string `json:"resource"`
	Methods  []string `json:"methods"`
	Method   string `json:"method"`
	Action   string `json:"action"`

	ResourceMatcher *regexp.Regexp
}

func (p *Permission) isAllowed(r *http.Request) bool {
	return p != nil && p.Action == ACTION_ALLOW
}

func (p *Permission) matchMethod(r *http.Request) bool {
	if p.Method != "" {
		return p.Method == "*" || strings.EqualFold(p.Method, r.Method)
	} else {
		for _, method := range p.Methods {
			if method == "*" || strings.EqualFold(method, r.Method) {
				return true
			}
		}
	}
	return false
}

func (p *Permission) compileMatcher() error {
	if p.Resource == "*" {
		return nil
	}

	reg, err := regexp.Compile("{([a-zA-Z0-9]+)}")

	if err != nil {
		return err
	}

	pattern := "^" + reg.ReplaceAllString(p.Resource, "([^/]+)") + "$"

	m, err := regexp.Compile(pattern)

	if err != nil {
		return err
	}

	p.ResourceMatcher = m
	return nil
}

func (p *Permission) matchResource(r *http.Request) bool {
	if p.Resource == "*" {
		return true
	}
	p.compileMatcher()

	url := strings.Split(r.URL.String(), "?")[0]

	//fmt.Printf("ACL: Resource := %s\n", p.Resource)
	//fmt.Printf("ACL: %s is matching %s ? %t\n", p.ResourceMatcher.String(), url, p.ResourceMatcher.MatchString(url))
	//fmt.Printf("ACL:\n")

	return p.ResourceMatcher.MatchString(url)
}
