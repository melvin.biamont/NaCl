package nacl

type Role struct {
	Name string `json:"role"`
	Permissions []Permission `json:"permissions"`
}